/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect;

import ifis.PMI.Config;
import java.io.File;
import nii.emotiondetect.Processing.EmotionVector;
import nii.emotiondetect.Processing.Sentence.Sentence;
import nii.emotiondetect.Processing.Sentence.SentenceFileIterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph
 */
public class TestSentenceFile {

    public TestSentenceFile() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //

    @Test
    public void testSentenceProcessor() throws Exception {
        Config.emoCaching = Config.CacheBehavior.UseCache;
        Config.emotionSeedFile = "./6emotion.csv";
        Config.fNormalizeBest = true;
        Config.fNormalizeBottom = true;
        Config.meanBehavior = Config.MeanBehavior.DROP_BOTTOM_20;
        Config.fParagraphMode = true;
        //
        {
            SentenceFileIterator iter = new SentenceFileIterator(new File("./src/test/files/testSentence.csv"));
            while (iter.hasNext()) {
                Sentence sentence = iter.next();
                EmotionVector vector = sentence.getEmotionVector();
                System.out.println(vector.toString());
            }
        }
        //

    }
}
