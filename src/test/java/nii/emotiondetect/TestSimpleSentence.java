/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect;

import ifis.PMI.Config;
import nii.emotiondetect.Processing.EmotionVector;
import nii.emotiondetect.Processing.Sentence.Sentence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph
 */
public class TestSimpleSentence {

    public TestSimpleSentence() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //

    @Test
    public void testSentenceProcessor() throws Exception {
        Config.emoCaching = Config.CacheBehavior.NoCache;
        Config.emotionSeedFile = "./6emotion.csv";
        Config.fNormalizeBest = true;
        Config.fNormalizeBottom = true;
        Config.meanBehavior = Config.MeanBehavior.DROP_BOTTOM_20;
        Config.fParagraphMode = true;
        Config.fDebugOutput = true;
        Config.meanBehavior = Config.MeanBehavior.WEIGHTING;
        /*
         {
         Sentence sentence = new Sentence("1;agony;horror;");
         EmotionVector vector = sentence.getEmotionVector();
         System.out.println(vector.toString());
         }
         //
         {
         Sentence sentence = new Sentence("1;!agony;!horror;");
         EmotionVector vector = sentence.getEmotionVector();
         System.out.println(vector.toString());
         }
         //
         {
         Sentence sentence = new Sentence("1;agony#horror;");
         EmotionVector vector = sentence.getEmotionVector();
         System.out.println(vector.toString());
         }
         {
         Sentence sentence = new Sentence("37; 35; hot; so; Why; usa; team; usa#team; usa#so \n");
         EmotionVector vector = sentence.getEmotionVector();
         System.out.println(vector.toString());
         } */
        {
            Sentence sentence = new Sentence("12; death|gymnastic*;");
            EmotionVector vector = sentence.getEmotionVector();
            System.out.println(vector.toString());
        }


    }
}
