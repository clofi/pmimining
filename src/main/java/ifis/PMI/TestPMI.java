/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ifis.PMI;

import java.io.File;
import java.util.List;
import nii.emotiondetect.Processing.PMIProcessor_EmotionVectors;
import nii.emotiondetect.Processing.EmotionVector;
import nii.emotiondetect.Processing.SeedWords;

/**
 *
 * @author Christoph
 */
public class TestPMI {

    /**
     * Just runs the the PMI scorer once
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        PMIProcessor pmi = PMIProcessor.getInstance();
        Config.fNormalizeBottom = false;
        Config.fNormalizeBest = true;
        System.out.println("Rome:Athens "+pmi.computePMI("Rome", "Athens"));
        System.out.println("Rome:New York "+pmi.computePMI("Rome", "New York"));
        /*
         Config.emotionSeedFile = "./data/20emotion.csv";
         Config.meanBehavior = Config.MeanBehavior.DROP_BOTTOM_20;
         Config.fDebugOutput = true;
         Config.emoCaching = Config.CacheBehavior.NoCache;
         Config.pmiCaching = Config.CacheBehavior.UseCache;
         // open index
         PMIProcessor_EmotionVectors pmi = PMIProcessor_EmotionVectors.getInstance();
         // open seed words
         SeedWords seeds = SeedWords.loadFromFile(new File(Config.emotionSeedFile));
         System.out.println(Config.getDescription());
         // double amu = pmi.computePMIwithEmotion("annoy", seeds.getSeedWordsFor("AMUSEMENT"));
         String testWord = "ang*";
         EmotionVector pmiValues = pmi.computePMIAllEmotions(testWord, seeds);
         System.out.println(">>" + testWord);
         List<EmotionVector.PMIEmoScore> orderedPmi = pmiValues.convertToOrderedEmoScores();
         for (EmotionVector.PMIEmoScore score : orderedPmi) {
         System.out.println(score);
         }
         */
    }
}
