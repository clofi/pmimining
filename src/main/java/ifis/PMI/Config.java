/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ifis.PMI;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import nii.utils.DbHelper;

/**
 *
 * @author Christoph
 */
public class Config {

    /**
     * Common Settings
     */
    public static boolean fDebugOutput = false; // verbose output
    public static CacheBehavior emoCaching = CacheBehavior.NoCache;
    public static CacheBehavior pmiCaching = CacheBehavior.NoCache;
    /**
     * PMI Evaluation Settings
     */
    public static boolean fGeometricMean = true; // root square otherwise
    public static boolean fNormalizeBest = true; // if using normalization, lower values are better, and best value is 1. if not, higher values are better. In any case, a 0 is a complete miss (no correlation, even for normalized version)
    public static boolean fNormalizeBottom = true;
    // normalized Best from paper: normalized (pointwise) mutual information in collocation extraction
    public static boolean fParagraphMode = true; // use paragraph index instead of article index
    public static double normalizationFactorForMisses = 1.5; // this controls the values when there is no shared occurances in PMI
    public static MeanBehavior meanBehavior = MeanBehavior.HARD;
    /**
     * Experiment settings
     */
    public static int navaWordstoTest = 11116;
    public static int onlyLogTopKemotions = 10;
    public static int PMIdistance = 25;
    /**
     * Files and indexes
     */
    public static String emotionSeedFile = "./data/20emotion.csv"; // valid values: 6emotion/20emotion/32emotion
    public static String wikipediaPath = "C:/Data/wikipedia/uncompressed"; // just used for indexing
    public static String navaWordFile = "./data/navaTestSet.csv"; // used for batch tests
    // use fParagraphmode to swithc between those two 
    private static Properties localconf;

    public enum MeanBehavior {

        HARD, DROP_BOTTOM_20, WEIGHTING
    }

    public enum CacheBehavior {

        UseCache, UpdateCache, NoCache
    }

    public static String getIndexPath() {
        if (fParagraphMode) {
            return getLocalProperty("indexDirP");
        } else {
            return getLocalProperty("indexDirA");
        }
    }

    public static int getNumOfThreads() {
        return Integer.valueOf(getLocalProperty("numOfThreads"));
    }

    public static String getDescription() {
        StringBuilder result = new StringBuilder();
        result.append((fNormalizeBest ? "nPMI" : "PMI")).append(" ").append((fParagraphMode ? "ParagraphIndex" : "ArticleIndex")).append(" ");
        result.append(emotionSeedFile).append(" ").append(navaWordFile);
        return result.toString();
    }

    /**
     * Returns a very short string representing the current configuration (for
     * using in DB)
     *
     * @return
     */
    public static String getDBModeIdentifier() {
        StringBuilder result = new StringBuilder();
        result.append((fParagraphMode ? "p" : "a")).append("_");
        result.append((fNormalizeBest ? "n" : "nn")).append("_");
        result.append((fNormalizeBottom ? "n" : "nn")).append("_");
        result.append((fGeometricMean ? "g" : "rs")).append("_");
        if (meanBehavior.equals(MeanBehavior.HARD)) {
            result.append("h");
        }
        if (meanBehavior.equals(MeanBehavior.DROP_BOTTOM_20)) {
            result.append("d20");
        }
        if (meanBehavior.equals(MeanBehavior.WEIGHTING)) {
            result.append("w");
        }
        return result.toString();
    }

    /**
     * Shortname of the used wordlist
     *
     * @return
     */
    public static String getListName() {
        return emotionSeedFile.replace("./", "").replace(".csv", "");
    }

    public static String resultPathPrefix() {
        return (fNormalizeBest ? "nPMI" : "PMI") + "_" + (emotionSeedFile.replace("./", "").replace(".csv", ""));
    }

    public static void resetDB()  {
        if (Config.emoCaching.equals(Config.CacheBehavior.UpdateCache)) {
            try {
                Connection conn = DbHelper.getPostgresConnection();
                PreparedStatement stmt = conn.prepareStatement("DELETE FROM emotion.emotionresults WHERE emotionlist=? AND mode=?");
                stmt.setString(1, Config.getListName());
                stmt.setString(2, Config.getDBModeIdentifier());
                stmt.executeUpdate();
                stmt.close();
                conn.close();
            } catch (Exception ex) {
                throw new RuntimeException("Cannot delete database", ex);
            }
        }
    }

    /**
     * Used to identify the origin of cached pmi results;
     *
     * @return
     */
    public static String getPmiAlgIdentifier() {
        StringBuilder result = new StringBuilder();
        result.append((Config.fNormalizeBest ? "n" : "v"));
        result.append((Config.fNormalizeBottom ? "n" : "v"));
        return result.toString();
    }

    private static String getLocalProperty(String name) {
        if (localconf == null) {
            try {
                localconf = new Properties();
                localconf.load(new FileReader("./localconf.property"));
            } catch (Exception ex) {
                throw new RuntimeException("Local Properties not found: " + ex.getMessage(), ex);
            }
        }
        return localconf.getProperty(name);
    }
}
