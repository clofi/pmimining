/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ifis.PMI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.util.Version;

/**
 *
 * @author Christoph
 */
public class Indexer {



    IndexWriter ixWriter;

    /**
     * Finds all files with a specific ending recursivly
     *
     * @param root
     * @param result
     * @param depth start with 0
     * @param fileEnding the file ending to filter in (whitelist)
     */
    public void findFiles(File root, Collection<File> result, int depth, String fileEnding) {
        File[] listOfFiles = root.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            String iName = listOfFiles[i].getName();
            if (listOfFiles[i].isFile()) {
                if (iName.endsWith("." + fileEnding.toUpperCase()) || iName.endsWith("." + fileEnding.toLowerCase())) {
                    result.add(listOfFiles[i]);
                }
            } else if (listOfFiles[i].isDirectory()) {
                findFiles(listOfFiles[i], result, depth + 1, fileEnding);
            }
        }

    }

    /**
     * Scans a file for articles, and sends them to the indexer
     *
     * @param file
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void indexFile(File file) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = reader.readLine();
        StringBuilder bodytext = new StringBuilder();
        String title = null;
        String paragraphTitle = "";
        int paragraphNo = 0;
        while (line != null) {
            if (line.startsWith("[[") && line.trim().endsWith("]]")) {
                // its a new article
                // sore the old
                if (!Config.fParagraphMode) {
                    indexArticle(title, bodytext);
                } else {
                    indexArticle(title, paragraphTitle, paragraphNo, bodytext);
                }
                // start a new article
                paragraphTitle = "";
                paragraphNo = 0;
                title = line.trim().replaceAll("\\[\\[", "").replaceAll("\\]\\]", "");
                bodytext = new StringBuilder();
            } else {
                // not a new article...maybe a new paragraph?
                if (Config.fParagraphMode && line.startsWith("===") && line.trim().endsWith("===")) {
                    indexArticle(title, paragraphTitle, paragraphNo, bodytext);
                    // new parapgraph
                    bodytext = new StringBuilder();
                    paragraphNo++;
                    paragraphTitle = line.replaceAll("===", "").trim();
                } else {
                    bodytext.append(line);
                }
            }
            line = reader.readLine();
        }
        ixWriter.commit();
    }

    /**
     * Stores an article in the lucene index
     *
     * @param title
     * @param article
     */
    private void indexArticle(String title, StringBuilder article) throws IOException {
        if (title != null) {
            //  System.out.println("Title: " + title);
            System.out.print(".");
            //    System.out.println("Article: " + article.toString());
            //
            Document document = new Document();

            document.add(new StringField("title", title, Field.Store.YES));
            document.add(new TextField("body", article.toString(), Field.Store.YES));
            ixWriter.addDocument(document);
        }
    }

    /**
     * Stores an article in the lucene index
     *
     * @param title
     * @param article
     */
    private void indexArticle(String articleTitle, String paragraphTitle, int paragraphNumber, StringBuilder text) throws IOException {
        if (articleTitle != null) {
            //  System.out.println("Title: " + title);
            //    System.out.println("Paragraph!" + paragraphTitle + "-" + articleTitle);
            //         System.out.println(text.toString());
            System.out.print(".");
            //    System.out.println("Article: " + article.toString());
            //
            Document document = new Document();

            document.add(new StringField("articleTitle", articleTitle, Field.Store.YES));
            document.add(new StringField("paragraphTitle", paragraphTitle, Field.Store.YES));
            document.add(new IntField("paragraphNo", paragraphNumber, Field.Store.YES));
            document.add(new TextField("body", text.toString(), Field.Store.YES));
            ixWriter.addDocument(document);
        }
    }

    /**
     * Main. Inits Lucene, scans folder and iterativly indexes files.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        // Get source files
        File sourcedir = new File(Config.wikipediaPath);
        Indexer indexer = new Indexer();
        Collection<File> txtFiles = new LinkedList<>();
        indexer.findFiles(sourcedir, txtFiles, 0, "TXT");
        //
        Analyzer analyzer = new EnglishAnalyzer(Version.LUCENE_44);
        IndexWriterConfig ixConfig = new IndexWriterConfig(Version.LUCENE_44, analyzer);
        Directory ixDir = new MMapDirectory(new File(Config.getIndexPath()));
        try (IndexWriter ixWriter = new IndexWriter(ixDir, ixConfig)) {
            indexer.ixWriter = ixWriter;
            ixWriter.deleteAll();
            for (File file : txtFiles) {
                System.out.println("Processing " + file.getAbsolutePath());
                indexer.indexFile(file);
            }
        }
    }
}
