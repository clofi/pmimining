/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ifis.PMI;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nii.emotiondetect.Processing.EmotionVector;
import nii.emotiondetect.Processing.SeedWords;
import nii.utils.DbHelper;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;

import org.apache.lucene.queryparser.complexPhrase.ComplexPhraseQueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TotalHitCountCollector;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.search.spans.SpanMultiTermQueryWrapper;
import org.apache.lucene.search.spans.SpanNearQuery;
import org.apache.lucene.search.spans.SpanQuery;
import org.apache.lucene.search.spans.SpanTermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.util.Version;

/**
 *
 * @author Christoph
 */
public class PMIProcessor {

    private static String field = "body";
    private static IndexSearcher searcher = null;
    private QueryParser parser;
    private QueryParser noFieldParser;
    private Analyzer analyzer;
    //
    private static PMIProcessor instance;

    public static PMIProcessor getInstance() {
        if (instance == null) {
            instance = createInstance();
        }
        return instance;
    }

    public static PMIProcessor createInstance() {
        try {
            if (searcher == null) {
                Directory iDir = new MMapDirectory(new File(Config.getIndexPath()));
                IndexReader iReader = DirectoryReader.open(iDir);
                searcher = new IndexSearcher(iReader);
            }
            return new PMIProcessor();
        } catch (IOException ex) {
            throw new RuntimeException("Cannot init PMIProcessor: " + ex.getMessage(), ex);
        }
    }

    public PMIProcessor() {
        analyzer = new EnglishAnalyzer(Version.LUCENE_44);
        parser = new QueryParser(Version.LUCENE_44, field, analyzer);
        noFieldParser = new QueryParser(Version.LUCENE_44, "", analyzer);
        //  parser = new QueryParser();

    }

    /**
     * Gets the lowest score possible for the given two words.
     *
     * @param word1
     * @param word2
     * @return
     * @throws IOException
     */
    private double computeLowestPMI(String word1, String word2) throws IOException {


        try {
            // build simple queries for the single words
            Query qTerm1 = parser.parse(word1);
            Query qTerm2 = parser.parse(word2);

            int jointHits = 0;
            //
            TotalHitCountCollector hitCollector = new org.apache.lucene.search.TotalHitCountCollector();
            searcher.search(qTerm1, hitCollector);
            int t1Hits = hitCollector.getTotalHits();
            hitCollector = new org.apache.lucene.search.TotalHitCountCollector();
            searcher.search(qTerm2, hitCollector);
            int t2Hits = hitCollector.getTotalHits();

            // repair special cases
            if (jointHits == 0) {
                jointHits = 1;
            }
            // compute PMI
            double npmi = 1.0 * jointHits / (1.0 * t1Hits * t2Hits); // non-log version
            double pmi = Math.log(npmi); // log version
            double norm = -1 * (Math.log(jointHits)); // normalization factor
            if (norm == 0) { // adjust for misses
                norm = -1 * (Math.log(Config.normalizationFactorForMisses));
            }

            double normalized; // perform normalization
            if (Config.fNormalizeBest) {
                normalized = pmi / norm;
            } else {
                normalized = pmi * -1;
            }

            // this should actually not happen anymore. Sets return value to zeero if something breaks.
            if (t1Hits * t2Hits == 0 || jointHits == 0 || norm == 0) {
                return 0;
            }
            return normalized;
        } catch (ParseException ex) {
            System.out.println(ex.toString());
            return 0;
        }
    }

    public int countSingleWordOccurence(String word1) throws ParseException, IOException {
        boolean q1wild = word1.contains("*");
        Query qTerm1;
        if (q1wild) {
            String word1woStar = word1.substring(0, word1.length() - 1);
            qTerm1 = parser.parse(QueryParser.escape(word1woStar) + "*");
        } else {
            qTerm1 = parser.parse(QueryParser.escape(word1));
        }

        TotalHitCountCollector hitCollector = new org.apache.lucene.search.TotalHitCountCollector();
        searcher.search(qTerm1, hitCollector);
        int hits = hitCollector.getTotalHits();
        return hits;
    }

    /**
     * Computes raw PMI with P(w1,w2)/(P(w1)*P(w2)). No log scores.
     *
     * @param word1
     * @param word2
     * @return
     * @throws IOException
     */
    public double computePMI(String word1, String word2) throws Exception {
        if (Config.pmiCaching.equals(
                Config.CacheBehavior.UseCache)) {
            Connection conn = DbHelper.getPostgresConnection();
            PreparedStatement stmt = conn.prepareStatement("SELECT value FROM emotion.pmivalues WHERE word1=? AND word2=? AND pmialg=?");
            stmt.setString(1, word1);
            stmt.setString(2, word2);
            stmt.setString(3, Config.getPmiAlgIdentifier());
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                double result = rs.getDouble(1);
                conn.close();
                return result;
            } else {
                conn.close();
            }
        }


        try {
            // build simple queries for the single words
            boolean q1wild = word1.contains("*");
            boolean q2wild = word2.contains("*");

            int t1Hits = this.countSingleWordOccurence(word1);
            int t2Hits = this.countSingleWordOccurence(word2);


            // build complex custom span query for the joint query
            SpanQuery st1;
            if (q1wild) {
                st1 = new SpanMultiTermQueryWrapper<>(new WildcardQuery(new Term(field, word1)));
            } else {
                st1 = new SpanTermQuery(new Term(field, noFieldParser.parse(QueryParser.escape(word1)).toString()));
            }
            SpanQuery st2;
            if (q2wild) {
                st2 = new SpanMultiTermQueryWrapper<>(new WildcardQuery(new Term(field, word2)));
            } else {

                st2 = new SpanTermQuery(new Term(field, noFieldParser.parse(QueryParser.escape(word2)).toString()));
            }
            Query qJoint = new SpanNearQuery(
                    new SpanQuery[]{
                        st1,
                        st2},
                    Config.PMIdistance,
                    true);

            // collect hits
            TotalHitCountCollector hitCollector = new org.apache.lucene.search.TotalHitCountCollector();
            searcher.search(qJoint, hitCollector);
            int jointHits = hitCollector.getTotalHits();
            //

            // repair special cases
            if (jointHits == 0) {
                jointHits = 1;
            }
            // compute PMI
            double npmi = 1.0 * jointHits / (1.0 * t1Hits * t2Hits); // non-log version
            double pmi = Math.log(npmi); // log version
            double worstPMI = Math.log(1.0 * 1 / (1.0 * t1Hits * t2Hits));
            double norm = -1 * (Math.log(jointHits)); // normalization factor
            if (norm == 0) { // adjust for misses
                norm = -1 * (Math.log(Config.normalizationFactorForMisses));
            }

            double normalized; // perform normalization best
            if (Config.fNormalizeBest) {
                normalized = pmi / norm;
                worstPMI = worstPMI / (-1 * Math.log(Config.normalizationFactorForMisses));
            } else {
                normalized = pmi * -1;
                worstPMI = worstPMI * -1;
            }

            if (Config.fNormalizeBottom) {
                normalized = normalized / worstPMI * 100;
            }


            // this should actually not happen anymore. Sets return value to zeero if something breaks.
            if (t1Hits * t2Hits == 0 || jointHits == 0 || norm == 0) {
                return 0;
            }
            // store
            if (Config.pmiCaching.equals(
                    Config.CacheBehavior.UseCache) || Config.pmiCaching.equals(
                    Config.CacheBehavior.UpdateCache)) {
                Connection conn = DbHelper.getPostgresConnection();
                PreparedStatement stmt = conn.prepareStatement("INSERT INTO emotion.pmivalues (word1,word2,pmialg,value) VALUES (?,?,?,?)");

                stmt.setString(1, word1);
                stmt.setString(2, word2);
                String mode = Config.getPmiAlgIdentifier();
                stmt.setString(3, mode);
                stmt.setDouble(4, normalized);
                stmt.executeUpdate();
                conn.close();
            }
            //
            return normalized;
        } catch (IndexOutOfBoundsException ex) {
            throw new RuntimeException("Cannot query for " + word1 + "  ||  " + word2 + " : " + ex.toString(), ex);

        } catch (ParseException ex) {
            System.out.println(ex.toString());
            return 0;
        }
    }

 
}
