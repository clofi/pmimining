/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.utils;

/**
 *
 * @author Christoph
 */
public class Util {

    public static double rootSquareMean(double[] values) {
        double result = 0;
        for (int i = 0; i < values.length; i++) {
            result += values[i] * values[i];
        }
        return Math.sqrt((1.0 / values.length) * result);
    }

    public static double weightedGeometricMean(double[] values, double[] weights) {
        double result = 0;
        double weightSum = 0;
        for (int i = 0; i < values.length; i++) {
            result += weights[i] * Math.log(values[i]);
            weightSum += weights[i];
        }
        return Math.exp(result / weightSum);
    }

    public static double normalize(double value, double min, double max) {
        return (value - min) / (max - min);
    }
}
