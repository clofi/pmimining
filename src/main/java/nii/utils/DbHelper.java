/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.utils;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import ifis.PMI.Config;

/**
 *
 * @author Christoph
 */
public class DbHelper {

    private static ComboPooledDataSource postgresPool;

    /**
     * Create a connection to the Äffchenbox-PGSQL. Use emotion as schema.
     *
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static Connection getPostgresConnection() throws SQLException, IOException {

        if (postgresPool == null) {

            postgresPool = new ComboPooledDataSource();
            try {
                postgresPool.setDriverClass("org.postgresql.Driver");
            } catch (PropertyVetoException ex) {
                Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
            //loads the jdbc driver 

            // local
            String url = "jdbc:postgresql://136.187.46.206:5432/miner";
            postgresPool.setJdbcUrl(url);
            postgresPool.setUser("miner");
            postgresPool.setPassword(getDbPassword(url, "miner"));
            postgresPool.setMaxStatements(50);
            postgresPool.setMinPoolSize(Config.getNumOfThreads() + 1);
        }

        // should be nioMemFS
        Connection conn = postgresPool.getConnection();
        // Delete all data if on update mode

        //
        return conn;
    }

    public static String getDbPassword(String url, String user) throws IOException {
        String homeDir = System.getProperty("user.home");
        String propertyName = url.replaceAll(":", "_").replaceAll("/", "") + "||" + user;
        File propertyFile = new File(homeDir + "//.javaDBpasswords.property");
        if (!propertyFile.canRead()) {
            throw new IOException("Needs DB password for user " + user + " on " + url + ". Put the password in a Java property file at "
                    + "location " + propertyFile.getPath() + " with the property name " + propertyName);
        }
        Properties properties = new Properties();
        properties.load(new FileReader(propertyFile));
        String result = properties.getProperty(propertyName);
        if (result == null) {
            throw new IOException("Needs DB password for user " + user + " on " + url + ". Put the password in a Java property file at "
                    + "location " + propertyFile.getPath() + " with the property name " + propertyName);
        }
        return result;
    }

}
