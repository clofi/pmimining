/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect;

import ifis.PMI.Config;

/**
 *
 * @author Christoph
 */
public class TestAllNava {

    public static void main(String[] args) throws Exception {
        // 6
        Config.fNormalizeBest = true;
        Config.emotionSeedFile = "./6emotion.csv";
        TestNavaWords.main(args);
        Config.fNormalizeBest = false;
        Config.emotionSeedFile = "./6emotion.csv";
        TestNavaWords.main(args);

        // 20
        Config.fNormalizeBest = true;
        Config.emotionSeedFile = "./20emotion.csv";
        TestNavaWords.main(args);
        Config.fNormalizeBest = false;
        Config.emotionSeedFile = "./20emotion.csv";
        TestNavaWords.main(args);

        // 32
        /*
        Config.fNormalizeBest = true;
        Config.emotionSeedFile = "./32emotion.csv";
        TestNavaWords.main(args);
        Config.fNormalizeBest = false;
        Config.emotionSeedFile = "./32emotion.csv";
        TestNavaWords.main(args);
        * */

    }
}
