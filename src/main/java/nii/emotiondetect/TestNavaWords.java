/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect;

import ifis.PMI.Config;
import nii.emotiondetect.Processing.SeedWords;
import nii.emotiondetect.Processing.EmotionVector;
import ifis.PMI.PMIProcessor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import nii.emotiondetect.Processing.PMIProcessor_EmotionVectors;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;

/**
 *
 * @author Christoph
 */
public class TestNavaWords {

    public static Collection<String> getWords() {
        Collection<String> words = new LinkedList<String>();
        words.add("love");
        words.add("hate");
        words.add("hilarious");
        words.add("fun");
        words.add("creepy");
        return words;
    }

    public static Collection<String> getWords(File file) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        Set<String> words = new HashSet<>();
        //
        String line = reader.readLine();
        while (line != null) {
            words.add(line.replaceAll("#", ""));
            line = reader.readLine();
        }
        //
        reader.close();
        return words;
    }

    /**
     * Just runs the the PMI scorer once
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        Config.resetDB();
        // open index
        Directory iDir = new MMapDirectory(new File(Config.getIndexPath()));


        PMIProcessor_EmotionVectors pmi = PMIProcessor_EmotionVectors.getInstance();

        // get NAVA words
        Collection<String> words = getWords(new File(Config.navaWordFile));

        // open seed words
        SeedWords seeds = SeedWords.loadFromFile(new File(Config.emotionSeedFile));

        PrintStream printer = new PrintStream(new File("./result/" + Config.resultPathPrefix() + ".txt"));
        printer.println(Config.getDescription());
        int maxWords = Math.min(Config.navaWordstoTest, words.size());

        Iterator<String> wordIter = words.iterator();
        // iterate over all nava words
        for (int i = 0; i < maxWords; i++) {
            String word = wordIter.next();
            EmotionVector pmiValues = pmi.computePMIAllEmotions(word, seeds);
            printer.println(">> " + word);
            List<EmotionVector.PMIEmoScore> orderedPmi = pmiValues.convertToOrderedEmoScores();
            // output emotions
            for (int o = 0; o < (Math.min(orderedPmi.size(), Config.onlyLogTopKemotions)); o++) {
                printer.println(orderedPmi.get(o));
            }
            printer.println();
            printer.flush();
        }
        printer.close();

    }
}
