/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect.Processing;

import ifis.PMI.PMIProcessor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import ifis.PMI.Config;

/**
 *
 * @author Christoph
 */
public class SeedWords {

    private List<String> orderedLabels = new LinkedList<>();
    private static SeedWords defaultWords;
    Map<String, Collection<String>> words = new TreeMap<>();
    Map<String, Integer> wordCountInWiki = new TreeMap<>();
    int maxCountInWiki = 0;

    private SeedWords() {
    }

    public Collection<String> getSeedWordsFor(String label) {
        return words.get(label);
    }

    public Map<String, Collection<String>> getWordMap() {
        return words;
    }

    public static void loadDefaultWordsFromFile(File file) throws FileNotFoundException, IOException {
        defaultWords = loadFromFile(file);
    }

    public static SeedWords getDefaultWords() {
        if (defaultWords == null) {
            try {
                loadDefaultWordsFromFile(new File(Config.emotionSeedFile));
            } catch (Exception ex) {
                throw new RuntimeException("Cannot load seed word file: " + ex.getMessage(), ex);
            }
        }
        return defaultWords;
    }

    public int getWikiWordcountFor(String word) throws Exception {
        Integer result = this.wordCountInWiki.get(word);
        if (result == null) {
            result = PMIProcessor.getInstance().countSingleWordOccurence(word);
            this.wordCountInWiki.put(word, result);
            if (result > this.maxCountInWiki) {
                maxCountInWiki = result;
            }
        }
        return result;
    }

    /**
     * Load seed words from CSV file
     *
     * @param file
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static SeedWords loadFromFile(File file) throws FileNotFoundException, IOException {
        SeedWords result = new SeedWords();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = reader.readLine();
            while (line != null) {
                String[] splits = line.split(";");
                String label = splits[0];
                result.orderedLabels.add(label);
                Collection<String> seedwords = new LinkedList<>();
                for (int i = 1; i < splits.length; i++) {
                    String word = splits[i];
                    if (word != null && word.length() > 0) {
                        //word = word.replace("*", "");
                        seedwords.add(word);

                    }
                }
                result.getWordMap().put(label, seedwords);
                //
                line = reader.readLine();
            }
        }
        return result;
    }

    public List<String> getOrderedLabels() {
        return this.orderedLabels;
    }

    public String getARFFheader() {
        StringBuilder result = new StringBuilder();
        for (String name : this.getOrderedLabels()) {
            if (name.equals("ANXIETY/WORRY")) {
                name = "WORRY";
            }
            if (name.equals("COMPASSION")) {
                name = "PITY";
            }
            name = name.toLowerCase().concat("Lex");
            String first = name.substring(0, 1).toUpperCase();
            name = first + name.substring(1);
            result.append("@attribute ").append(name).append(" REAL\n");
        }
        result.append("@attribute ").append("NoEmotionLex").append(" REAL\n");
        return result.toString();
    }
}
