/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect.Processing;

import ifis.PMI.PMIProcessor;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import ifis.PMI.Config;
import nii.emotiondetect.Processing.EmotionVector;
import nii.emotiondetect.Processing.SeedWords;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;

/**
 *
 * @author Christoph
 */
public class PMIProcessor_EmotionVectors {

    private static PMIProcessor_EmotionVectors instance = null;
    private PMIProcessor processor = null;

    public static PMIProcessor_EmotionVectors getInstance() {
        if (instance == null) {
            instance = new PMIProcessor_EmotionVectors(PMIProcessor.createInstance());
        }
        return instance;
    }

    private PMIProcessor_EmotionVectors(PMIProcessor pmiProc) {
        processor = pmiProc;
    }

    public static PMIProcessor_EmotionVectors createInstance(PMIProcessor pmiProc) {
        return new PMIProcessor_EmotionVectors(pmiProc);
    }

    /**
     * Computes PMI between a word and a collection of seed words belonging to a
     * specific emotion class.
     *
     * @param word
     * @param seedGroup
     * @return
     */
    public double computePMItoEmotion(String word, String emotionLabel, Collection<String> seedGroup) throws IOException, ParseException, Exception {
        // debug
        if (Config.fDebugOutput) {
            System.out.print("    PMI(" + word + "," + emotionLabel + "): ");
        }
        // compute all pmi values   
        double[] pmis = new double[seedGroup.size()];
        int i = 0;
        for (String sWord : seedGroup) {
            double pmi = PMIProcessor.getInstance().computePMI(word, sWord);
            pmis[i] = pmi;
            // debug
            if (Config.fDebugOutput) {
                System.out.print(" " + sWord + "(");
                System.out.printf("%.2f", pmi);
                //     System.out.print("/");
                //      System.out.printf("%.2f", computeLowestPMI(word, sWord));
                System.out.print(")");
            }
            i++;
        }
        // remove bad values

        if (Config.meanBehavior == Config.MeanBehavior.DROP_BOTTOM_20) {
            int ignoreBottomK = pmis.length / 5; // how many bad values should be ignored?
            Arrays.sort(pmis);
            pmis = Arrays.copyOfRange(pmis, 0, pmis.length - ignoreBottomK);
        }
        double result = 0;
        if (Config.meanBehavior == Config.MeanBehavior.WEIGHTING) {
            double[] weights = new double[seedGroup.size()];
            int o = 0;
            for (String eword : seedGroup) {
                weights[o++] = (1.0 * SeedWords.getDefaultWords().getWikiWordcountFor(eword)) / (SeedWords.getDefaultWords().maxCountInWiki * 1.0);
            }
            result = nii.utils.Util.weightedGeometricMean(pmis, weights);
        } else {
            // compute mean

            if (Config.fGeometricMean) {
                result = StatUtils.geometricMean(pmis);
            } else {
                result = nii.utils.Util.rootSquareMean(pmis);
            }
        }

        //
        // debug
        if (Config.fDebugOutput) {
            System.out.println("   Overall: " + result);
        }
        return result;
    }

    /**
     * Computes all all PMI scores between a given word and all emotions in a
     * seed file.
     *
     * @param word
     * @param seeds
     * @return a map between emotion labels and PMI scores
     */
    public EmotionVector computePMIAllEmotions(String word, SeedWords seeds) throws IOException, ParseException, Exception {
        if (Config.emoCaching.equals(Config.CacheBehavior.UseCache)) {
            EmotionVector cached = EmotionVector.loadFromDB(word);
            if (cached != null) {
                return cached;
            }
        }
        Map<String, Double> pmiValues = new TreeMap<>();
        for (Map.Entry<String, Collection<String>> entry : seeds.getWordMap().entrySet()) {
            double pmi = computePMItoEmotion(word, entry.getKey(), entry.getValue());
            pmiValues.put(entry.getKey(), pmi);
        }
        EmotionVector result = new EmotionVector(word, pmiValues);
        if (!Config.emoCaching.equals(Config.CacheBehavior.NoCache)) {
            result.storeToDB();
        }
        return result;
    }

}
