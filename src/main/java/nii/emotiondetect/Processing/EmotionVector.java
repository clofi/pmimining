/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect.Processing;

import ifis.PMI.PMIProcessor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import ifis.PMI.Config;
import nii.utils.DbHelper;
import nii.utils.Util;

/**
 *
 * @author Christoph
 */
public class EmotionVector {

    private String word;
    private Map<String, Double> scores = new TreeMap<>();
    private static PreparedStatement insertStmt = null;
    private static PreparedStatement loadStmt = null;

    public EmotionVector(String word, Map<String, Double> scores) {
        this.scores.putAll(scores);
        this.word = word;
    }

    /**
     * Empty emotion vector;
     *
     * @param word
     */
    public EmotionVector(String word) {
        this.scores.putAll(scores);
        this.word = word;
    }

    static EmotionVector loadFromDB(String word) throws Exception {
        try {
       //     synchronized (EmotionVector.class) {
                Connection conn = DbHelper.getPostgresConnection();

                PreparedStatement loadStmt = conn.prepareStatement("SELECT emotion, value FROM emotion.emotionresults WHERE emotionlist=? AND word=? AND mode=?");

                loadStmt.setString(1, Config.getListName());
                loadStmt.setString(2, word);
                loadStmt.setString(3, Config.getDBModeIdentifier());
                ResultSet rs = loadStmt.executeQuery();
                Map<String, Double> pmiMap = new TreeMap<>();
                //
                List<String> allEmotionsInSeed = new LinkedList<>(SeedWords.getDefaultWords().getOrderedLabels());
                //
                while (rs.next()) {
                    String emo = rs.getString("emotion");
                    Double value = rs.getDouble("value");
                    if (SeedWords.getDefaultWords().getSeedWordsFor(emo) != null) {
                        pmiMap.put(emo, value);
                        allEmotionsInSeed.remove(emo);
                    }
                }
                rs.close();
                //
                if (pmiMap.isEmpty()) {
                    return null;
                }
                //
                for (String leftOutEmo : allEmotionsInSeed) {
                    Double value = PMIProcessor_EmotionVectors.getInstance().computePMItoEmotion(word, leftOutEmo, SeedWords.getDefaultWords().getSeedWordsFor(leftOutEmo));
                    pmiMap.put(leftOutEmo, value);
                }
                //

                conn.close();
                EmotionVector result = new EmotionVector(word, pmiMap);
                return result;
     //       }
        } catch (Exception ex) {
            Logger.getLogger(EmotionVector.class.getName()).severe("Execption querying for emovector for " + word + " : " + ex.getMessage());
            return null;
        }

    }

    public static EmotionVector computeAverageVector(String label, List<EmotionVector> vectors) {
        EmotionVector result = new EmotionVector(label, new TreeMap<String, Double>());
        // for each vector
        for (EmotionVector vector : vectors) {
            // for each emotion
            for (Entry<String, Double> entry : vector.scores.entrySet()) {
                String key = entry.getKey();
                Double oldScore = result.scores.get(key);
                if (oldScore == null) {
                    oldScore = 0.0;
                }
                oldScore += entry.getValue();
                result.scores.put(entry.getKey(), oldScore);
            }
        }
        // adjust by number of vectors
        for (Entry<String, Double> entry : result.scores.entrySet()) {
            Double score = entry.getValue() / (1.0 * vectors.size());
            String key = entry.getKey();
            result.scores.put(key, score);
        }

        return result;
    }

    /**
     * If in update mode, store this vector
     */
    public void storeToDB() {
      //  synchronized (EmotionVector.class) {
            try {

                Connection conn = DbHelper.getPostgresConnection();
                PreparedStatement insertStmt = DbHelper.getPostgresConnection().prepareStatement("INSERT INTO emotion.emotionresults (emotionlist, word, mode, emotion, value) VALUES (?,?,?,?,?)");

                for (Map.Entry<String, Double> entry : scores.entrySet()) {
                    insertStmt.setString(1, Config.getListName());
                    insertStmt.setString(2, this.word);
                    insertStmt.setString(3, Config.getDBModeIdentifier());
                    insertStmt.setString(4, entry.getKey());
                    insertStmt.setDouble(5, entry.getValue());
                    insertStmt.executeUpdate();
                }
                // stmt.close();
                conn.close();

            } catch (Exception e) {
                // eat exception
                //  throw new RuntimeException("Cannot store emotion vector", e);
            }
      //  }
    }

    /**
     * Converts a result from computePMIAllEmotions into a ordered list for
     * display purposes (best values first : low for normalized, high for
     * unnormalized). Removes all 0 values.
     *
     * @param pmiScores
     * @return
     */
    public List<PMIEmoScore> convertToOrderedEmoScores() {
        List<PMIEmoScore> result = new LinkedList<>();
        for (Map.Entry<String, Double> entry : scores.entrySet()) {
            result.add(new PMIEmoScore(entry.getKey(), entry.getValue()));
        }
        Collections.sort(result);
        return result;
    }

    /**
     * Pretty print of the emotion vector
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Emotions(").append(word).append(")\n");
        List<EmotionVector.PMIEmoScore> orderedPmi = this.convertToOrderedEmoScores();
        for (EmotionVector.PMIEmoScore score : orderedPmi) {
            result.append(score.toString()).append("\n");
        }
        return result.toString();
    }

    public String toARFFresult() {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        boolean allZero = true;
        for (String label : SeedWords.getDefaultWords().getOrderedLabels()) {
            if (!first) {
                result.append(",");
            } else {
                first = false;
            }
            Double score = this.scores.get(label);
            if (score != null) {
                score = Util.normalize(score, 100, 1);
                if (score > 0) {
                    allZero = false;
                }
            } else {
                score = 0.0;
            }
            if (!(Config.fNormalizeBest == true && Config.fNormalizeBottom == true)) {
                throw new RuntimeException("toARFFResult does not support non-normalized values");
            }
            result.append(score);
        }
        if (allZero) {
            result.append(", 1");
        } else {
            result.append(", 0");
        }
        return result.toString();
    }

    /**
     * Simply presents a emotion label and a PMI score.
     */
    public static class PMIEmoScore implements Comparable<PMIEmoScore> {

        private Double score;
        private String label;

        public PMIEmoScore(String label, double score) {
            this.label = label;
            this.score = score;
        }

        @Override
        public int compareTo(PMIEmoScore o) {
            return score.compareTo(o.score);
        }

        @Override
        public String toString() {
            return label + " : " + score;
        }

        public Double getScore() {
            return score;
        }

        public String getLabel() {
            return label;
        }
    }
}
