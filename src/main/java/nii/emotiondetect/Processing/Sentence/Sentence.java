/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect.Processing.Sentence;

import java.util.LinkedList;
import java.util.List;
import nii.emotiondetect.Processing.EmotionVector;
import ifis.PMI.PMIProcessor;
import nii.emotiondetect.Processing.PMIProcessor_EmotionVectors;
import nii.emotiondetect.Processing.SeedWords;

/**
 *
 * @author Christoph
 */
public class Sentence {

    private int id;
    private String rawString;
    private List<IWord> words;
    private PMIProcessor_EmotionVectors pmiProcessor = null;

    public Sentence(String rawString) {
        this.rawString = rawString;
        rawString = rawString.replaceAll("\u0000", "");
        //   rawString = rawString.replace(",", ";");
        //
        words = new LinkedList<>();
        String[] cols = rawString.split(";");
        id = Integer.valueOf(cols[0]);

        for (int i = 1; i < cols.length; i++) {
            cols[i] = cols[i].trim().toLowerCase();
            if (!cols[i].isEmpty()) {
                cols[i] = cols[i].replaceAll("\\|\\|", "|");
                if (cols[i].startsWith("|")) {
                    cols[i] = cols[i].substring(1, cols[i].length() - 1);
                }
                //
                if (cols[i].contains("|")) {
                    // Dependent
                    IWord word = new DependentWord(cols[i]);
                    this.removeIfContained(cols[i]);
                    words.add(word);

                } else if (cols[i].startsWith("!")) {
                    // negated
                    IWord word = new NegatedWord(cols[i]);
                    words.add(word);
                } else {
                    // simple
                    IWord word = new SimpleWord(cols[i]);
                    words.add(word);
                }
            }
        }
    }

    public EmotionVector getEmotionVector() throws Exception {
        LinkedList<EmotionVector> wordEmotions = new LinkedList<>();
        for (IWord word : words) {
            EmotionVector wordEmo = word.getEmotionVector((pmiProcessor == null) ? PMIProcessor_EmotionVectors.getInstance() : this.pmiProcessor);
            if (wordEmo != null) {
                wordEmotions.add(wordEmo);
            }
        }
        //

        //
        EmotionVector result = EmotionVector.computeAverageVector(this.toString(), wordEmotions);
        return result;
    }

    /**
     * If you don't do this, the default one will be used
     *
     * @param pmiProc
     */
    public void setPMIProcessor(PMIProcessor_EmotionVectors pmiProc) {
        this.pmiProcessor = pmiProc;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (IWord word : words) {
            result.append(word.toString()).append("; ");
        }
        String sresult = result.toString();
        return sresult;
    }

    /**
     * Removed all occurences of the given simple word
     *
     * @param simpleWord
     */
    public void removeIfContained(String simpleWord) {
        for (IWord word : this.words) {
            if (word.toString().equals(simpleWord)) {
                words.remove(simpleWord);
            }
        }
    }
}
