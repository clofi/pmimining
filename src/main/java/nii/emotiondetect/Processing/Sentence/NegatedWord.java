/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect.Processing.Sentence;

import nii.emotiondetect.Processing.EmotionVector;
import ifis.PMI.PMIProcessor;
import nii.emotiondetect.Processing.PMIProcessor_EmotionVectors;
import nii.emotiondetect.Processing.SeedWords;

/**
 *
 * @author Christoph
 */
public class NegatedWord implements IWord {

    private String word;

    NegatedWord(String rawString) {
        this.word = rawString.replace("!", "");
    }

    @Override
    public EmotionVector getEmotionVector(PMIProcessor_EmotionVectors pmiP) throws Exception {
        return null;
     //   return PMIProcessor.getInstance().computePMIAllEmotions(word, SeedWords.getDefaultWords());
    }

    @Override
    public String toString() {
        return "!" + word;
    }
}
