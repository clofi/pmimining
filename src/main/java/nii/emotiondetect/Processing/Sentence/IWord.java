/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect.Processing.Sentence;

import nii.emotiondetect.Processing.EmotionVector;
import ifis.PMI.PMIProcessor;
import nii.emotiondetect.Processing.PMIProcessor_EmotionVectors;

/**
 *
 * @author Christoph
 */
public interface IWord {

    public EmotionVector getEmotionVector(PMIProcessor_EmotionVectors pmiP) throws Exception;

    @Override
    public String toString();
}
