/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect.Processing.Sentence;

import java.util.LinkedList;
import java.util.List;
import nii.emotiondetect.Processing.EmotionVector;
import ifis.PMI.PMIProcessor;
import nii.emotiondetect.Processing.PMIProcessor_EmotionVectors;
import nii.emotiondetect.Processing.SeedWords;

/**
 *
 * @author Christoph
 */
public class DependentWord implements IWord {

    private String word;
    private String influenceWord;

    DependentWord(String rawString) {
        String[] splits = rawString.split("\\|");
        word = splits[0];
        influenceWord = splits[1];
    }

    @Override
    public EmotionVector getEmotionVector(PMIProcessor_EmotionVectors pmiP) throws Exception {
        List<EmotionVector> list = new LinkedList<>();
        list.add(pmiP.computePMIAllEmotions(word, SeedWords.getDefaultWords()));
        list.add(pmiP.computePMIAllEmotions(influenceWord, SeedWords.getDefaultWords()));
        return EmotionVector.computeAverageVector(this.toString(), list);
    }

    @Override
    public String toString() {
        return word + "|" + influenceWord;
    }
}
