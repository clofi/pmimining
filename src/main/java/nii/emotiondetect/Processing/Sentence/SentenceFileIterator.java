/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect.Processing.Sentence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christoph
 */
public class SentenceFileIterator implements Iterator<Sentence> {

    private File file;
    private BufferedReader reader;
    private String nextLine;

    public SentenceFileIterator(File file) throws FileNotFoundException, IOException {
        this.file = file;
        this.reader = new BufferedReader(new FileReader(file));
        this.nextLine = reader.readLine();
    }

    @Override
    public boolean hasNext() {
        synchronized (this) {
            if (nextLine == null) {
                return false;
            } else {
                return true;
            }
        }
    }

    @Override
    public Sentence next() {
        synchronized (SentenceFileIterator.class) {
            if (nextLine == null) {
                return null;
            } else {
                try {
                    Sentence result = new Sentence(nextLine);
                    this.nextLine = reader.readLine();
                    return result;
                } catch (IOException ex) {
                    throw new RuntimeException("Cannot read line: " + ex.toString(), ex);
                }
            }
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void close() throws IOException {
        reader.close();
    }
}
