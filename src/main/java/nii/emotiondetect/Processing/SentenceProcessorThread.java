/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect.Processing;

import ifis.PMI.PMIProcessor;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import nii.emotiondetect.Processing.Sentence.Sentence;
import nii.emotiondetect.Processing.Sentence.SentenceFileIterator;

/**
 *
 * @author Christoph
 */
public class SentenceProcessorThread implements Runnable {

    private SentenceFileIterator fileIterator;
    private int threadNumber;
    private PrintWriter out;
    private PMIProcessor_EmotionVectors pmiProc;
    private Connection jdbcConn;

    public SentenceProcessorThread(SentenceFileIterator fileIterator, int threadNumber, PrintWriter out) {
        this.fileIterator = fileIterator;
        this.threadNumber = threadNumber;
        this.out = out;
        this.pmiProc = PMIProcessor_EmotionVectors.createInstance(PMIProcessor.createInstance());
    }

    @Override
    public void run() {
        while (fileIterator.hasNext()) {

            Sentence sentence = fileIterator.next();
            sentence.setPMIProcessor(pmiProc);
            if (sentence != null) {
                try {
                    EmotionVector vector = sentence.getEmotionVector();
                    System.out.println(threadNumber + ":" + vector.toString());
                    out.println(vector.toARFFresult());
                    out.flush();
                    System.gc();
                } catch (Exception ex) {
                    Logger.getLogger(SentenceProcessorThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }
}
