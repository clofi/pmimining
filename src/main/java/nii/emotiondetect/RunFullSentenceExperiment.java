/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nii.emotiondetect;

import ifis.PMI.Config;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import nii.emotiondetect.Processing.EmotionVector;
import nii.emotiondetect.Processing.SeedWords;
import nii.emotiondetect.Processing.Sentence.Sentence;
import nii.emotiondetect.Processing.Sentence.SentenceFileIterator;
import nii.emotiondetect.Processing.SentenceProcessorThread;

/**
 *
 * @author Christoph
 */
public class RunFullSentenceExperiment {

    public static void main(String[] args) throws FileNotFoundException, IOException, Exception {
        Config.emoCaching = Config.CacheBehavior.UseCache;
        Config.pmiCaching = Config.CacheBehavior.UseCache;
        Config.emotionSeedFile = "./8emotion.csv";
        Config.fNormalizeBest = true;
        Config.fNormalizeBottom = true;
        Config.meanBehavior = Config.meanBehavior.HARD;
        Config.fParagraphMode = true;
        System.out.println(SeedWords.getDefaultWords().getARFFheader());
        //
        String datafile = "norm_SREC_NavaDep";
        SentenceFileIterator iter = new SentenceFileIterator(new File("./data/" + datafile + ".csv"));
        ExecutorService es = Executors.newCachedThreadPool();
        //
        PrintWriter out = new PrintWriter(new FileWriter(new File("./result/" + "C_" + datafile + "_" + Config.getDBModeIdentifier() + "_" + Config.getListName() + ".arff")));
        out.println("RELATION " + datafile + "_classification\n"); //
        out.println(SeedWords.getDefaultWords().getARFFheader());
        out.println("\n@data");
        out.flush();

        for (int i = 0; i < Config.getNumOfThreads(); i++) {
            es.execute(new SentenceProcessorThread(iter, i, out));
        }
        es.shutdown();
        es.awaitTermination(10, TimeUnit.DAYS);
        out.close();
        //
    }
}
