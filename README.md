# README #

How to get this running?

The contained project is using the Netbeans Maven layout, therefore I would HIGHLY recommend to also use Netbeans when working with this project.

You can see an test example in ifis.PMI.TextPMI.java

In order to run this project, you first have to prepare a Lucene index. For this, you need:
- Text files (e.g., use wikipedia here. Extract the zipfile to a folder)

- An indexer (There is a indexer for the the mentioned wikipedia files ifis.PMI.Indexer

- You need to update the path to the sourcefiles in ifis.PMI.Config.java#wikipediaPath

- You can update the path to the index file (which will be written by the inder) in localconf.property

- Run the indexer

Afterwards, you can run the test file. 

- In Config.java, you can also enable PMI normalization by setting fNormalizeBest and fNormalizeBottom. Default PMI uses both set to false. 

- best:false bottom:false:: high relatedness has a value close to but smaller than 0, open negative scale (low relatedness is big negative number)

- best:true bottom:false:: best possible relatedness is 1, worst is high positive number

- best:true: bottom:true:: best value 1, worst value 100